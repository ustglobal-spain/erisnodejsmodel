var ethereum = require('./platforms/ethereum')
var solc = require('solc')

var source = "contract greeter{    /* define variable greeting of the type string */    string greeting;     event evt_newData(string data);    /* this runs when the contract is executed */    function greeter(string _greeting) public {        greeting = _greeting;         evt_newData(greeting);    }    /* main function */    function greet() constant returns (string) {        return greeting;    }    function setGreet(string _greet){    greeting = _greet;  evt_newData(_greet);    }}"

var greeterCompiled = solc.compile(source);

var abi = JSON.parse(greeterCompiled.contracts.greeter.interface)
var bytecode = greeterCompiled.contracts.greeter.bytecode
var address = '0xd87354f663f7925d0e772678734ae1d0fb6ce072'

var contractInstance = ethereum.eventWatcher(abi, address, 'evt_newData', eventCallback)




function eventCallback(err, result) {
  if (err) {
    console.log(err)
    return;
  }
  console.log(result)
}
