var ethereum = require('./platforms/ethereum')
var solc = require('solc')

var source = "contract greeter{    /* define variable greeting of the type string */    string greeting;     event evt_newData(string data);    /* this runs when the contract is executed */    function greeter(string _greeting) public {        greeting = _greeting;         evt_newData(greeting);    }    /* main function */    function greet() constant returns (string) {        return greeting;    }    function setGreet(string _greet){    greeting = _greet;  evt_newData(_greet);    }}"

var greeterCompiled = solc.compile(source);

var abi = JSON.parse(greeterCompiled.contracts.greeter.interface)
var bytecode = greeterCompiled.contracts.greeter.bytecode
var myres;
var array = [((error, result) =>
  console.log(result)
) ]

ethereum.unlockAccount('0x8ad3eba65259fc0cf207eb881049fde93b65b646', 'password', 90)
ethereum.sendCompiledContract(bytecode, abi, "greeter", "0x8ad3eba65259fc0cf207eb881049fde93b65b646")
.then(res => {
}).catch(err => {
   log.error(err)
})
 ethereum.unlockAccount('0x8ad3eba65259fc0cf207eb881049fde93b65b646', 'password', 10)
 ethereum.callContractFunction(abi, "0xd87354f663f7925d0e772678734ae1d0fb6ce072", 'greet', [], '')
 ethereum.callContractFunction(abi, "0xd87354f663f7925d0e772678734ae1d0fb6ce072", 'setGreet', ["holi"], '0x8ad3eba65259fc0cf207eb881049fde93b65b646')
