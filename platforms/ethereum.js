'use strict';
var web3_extended = require('web3_extended');
var web3;
var appRoot = require('app-root-path');
var config = require(appRoot + '/configuration.js').config.eth;
var Log = require('log'),
  log = new Log(config.logLevel); // debug, info, error
var request = require('request');
var account = config.accountData.address
var myGas = config.options.gas

var initialize = function (host) {
  var options = {
    host: host,
    ipc: false,
    personal: true,
    admin: true,
    debug: true
  	};
  web3 = web3_extended.create(options);
}

initialize(config.node0.url);


function retryDeployContract(myContractFactory, contract, name){
  return new Promise((resolve, reject) => {
    var intervalObject = setInterval(function(){
      log.debug('Trying deploy contract ...');
      unlockAccount(account, config.accountData.password, 0);
      myContractFactory.new({from: account,
       data: '0x' + contract, gas: myGas}, function(error, deployedContract){
        if(!error) {
          if(!deployedContract.address) {
          } else {
            myGas = config.options.gas
            account = config.accountData.address
            log.debug("Contract mined! Address: " + deployedContract.address);
            var result = {
              name: name,
              platform: 'eth',
              address: deployedContract.address
            }
            clearInterval(intervalObject);
            log.info("Resolving promise...")
            resolve(result);
          }
        }else{
          log.error("Contract not deployed " + error);
          reject(error);
        }
      });
    }, config.deployIntervalDelay);
  });
}

function sendCompiledContract(contract, abi, name){
  log.info('Sending compiled contract');
  return new Promise((resolve, reject) => {
    var myContractFactory = web3.eth.contract(abi);
    retryDeployContract(myContractFactory, contract, name)
    .then(deployed => {
      resolve(deployed)
    });
  });
}


function parseContractResponse (functionAbi, rawResponse){
  if (typeof(rawResponse) !== 'object' ){ //single element, wrapping it on a object to homogenize
    rawResponse = [rawResponse];
  }
  var result = {};
  functionAbi.outputs.forEach( (theOutput, theIndex) => {
    var rawResponseItem = rawResponse[theIndex];
    var procesedResponse = null ;
    var procesor = '';
    switch (theOutput.type){
      case 'bytes32':
        procesor = 'hex2utf8';
        procesedResponse = web3.toUtf8(rawResponseItem);
        break;
      case 'uint':
      case 'uint256':
        procesor = 'number';
        procesedResponse = parseInt(rawResponseItem);
        break;
      default:
        procesor = 'none';
        procesedResponse = rawResponseItem;
    }
    log.debug('[ethdb_model] '+'response att '+ theOutput.name + ': ' + rawResponseItem + ' -> ' + procesedResponse + ' (' + procesor + ')');
    result[theOutput.name] = procesedResponse ;
  });
  return result;
}

function utf8ToHex (salida, maxlength){
  var texto = salida;
  if (maxlength){
    texto = salida.substr(0, maxlength);
  }
  var intermediate = new Buffer(texto, 'utf8').toString('hex');
  return intermediate;
}

function hex2Utf8 (hex){
  var cadena = hex.replace(/^(00)+/, ''); //remove leading zeros
  return new Buffer(cadena, 'hex').toString('utf8').replace(/\u0000/g, "");
}

function getContractInstance(abi, address){
  var contractObject = web3.eth.contract(abi);
  return contractObject.at(address);
}

function unlockAccount(account, password, time){
  log.info('Unlocking account');
  web3.personal.unlockAccount(account, password, 0)
}

function createArgumentsContract(parameters, account){
  log.debug("Creating arguments contract with parameters.... " + JSON.stringify(parameters))
  log.debug("Deploying with account " + JSON.stringify(account))
 parameters.push({from:account,  gas: config.options.gas });
 return parameters;
}

function createParamsArray( functionAbi, parameters){
  var inputs = functionAbi.inputs;
  var result = inputs.map ( input => {
    var rawValue = parameters[input.name];
    var finalValue = null;
    if (typeof(rawValue) !== 'undefined'){
      switch (input.type) {
        case 'bytes32':
          log.debug('[ethdb_model] '+'type bytes32');
          if (rawValue.length === 0 ){
            rawValue = ' ';
          }
          //finalValue = utf8ToHex(rawValue,32);
          finalValue = web3.toHex(rawValue);
          break;
        default:
          finalValue = rawValue;
      }
      log.debug('[ethdb_model] '+'att [' + input.name + '] = ' + rawValue + ' -> ' + finalValue );
    } else {
      log.debug('[ethdb_model] '+'att [' + input.name + '] not provided !!');
    }
    return finalValue;
  });
  log.debug('[ethdb_model] '+'atts '+JSON.stringify(result));
  return result;
}
function getTheFunctionAbi (abi, functionName){
  log.debug('[ethdb_model] '+'search funcion ABI');
  var result = abi.find ( (element) => {return element.name === functionName && element.type === 'function';});
  log.debug('[ethdb_model] '+'found function abi: ' + result);
  return result;
}
function callContractFunction(abi, address, functionName, parameters, account){
  return new Promise((resolve, reject) => {
    var functionAbi = getTheFunctionAbi(abi, functionName)
    log.info('Calling contract function ');
    log.debug('Parameters + ', parameters)
    var contractInstance = getContractInstance(abi, address);
    unlockAccount(config.accountData.address, 'password', 0)
    if (functionAbi){
      var array = createParamsArray(functionAbi, parameters)
      var argumentsContract = createArgumentsContract(array, config.accountData.address)
      argumentsContract.push(function(error, response){
        if (error){
          log.error ('[ethdb_model] '+'contract function call resulted in error: '+ error);
          reject(error);
        }
        log.info('[ethdb_model] '+'response: ' + response);
        if (typeof(response)==='undefined'){
          resolve();
        }
        else{
          var proccessedResponse = parseContractResponse(functionAbi, response);
          resolve(proccessedResponse);
        }
      });
      contractInstance[functionName].apply(contractInstance, argumentsContract);
    }
    else {
      log.error('[ethdb_model] '+'function not found in contract or contract definition');
      reject('function not found in contract or contract definition');
    }
  })

}

function transformValue(value, type){
  var procesedResponse = '';
  switch (type){
    case 'bytes32':
      procesedResponse = web3.toUtf8(value);
      break;
    case 'uint':
    case 'uint256':
      procesedResponse = parseInt(value);
      break;
    default:
      procesedResponse = value;
  }
  return procesedResponse;
}

function processEventArgs(eventArgs, eventInputs){
  var processedEventArgs = {};
  for(var i in eventArgs){
    var name = i;
    for(var j in eventInputs){
      if(name === eventInputs[j].name){
        var type = eventInputs[j].type;
        var value = eventArgs[i];
        var processedValue = transformValue(value, type);
        processedEventArgs[name] = processedValue;
      }
    }
  }
  return processedEventArgs;
}

function processEvent(abi, evento){
  var eventName = evento.event;
  var args = evento.args;
  var processedEvent = {};
  processedEvent.event = eventName;
  processedEvent.address = evento.address;
  for(var i in abi){
    if(eventName === abi[i].name){
      var processedArgs = processEventArgs(args, abi[i].inputs);
      processedEvent.args = processedArgs;
    }
  }
  return processedEvent;
}

function eventWatcher(abi, contractAddress, eventName, eventCallback){
  var subscription;
  function startCallback(error, eventSub){
       if(error){
        log.info('Retrying subscription...');
       }
       subscription = eventSub;
  }
  var contractInstance = getContractInstance(abi, contractAddress)
  log.info('[ethdb_model] '+`Listening event ${eventName} .....`);
  var preProcess = function(err, evento){
    if(!err){
      var eventProcessed = processEvent(abi, evento);
      eventCallback(null, eventProcessed);
    }
  };
  contractInstance[eventName](startCallback, preProcess);
};


module.exports = {
  initialize,
  sendCompiledContract,
  callContractFunction,
  getContractInstance,
  unlockAccount,
  eventWatcher
}
