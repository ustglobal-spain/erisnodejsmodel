'use strict';

var appRoot = require('app-root-path');
var configGen = require(appRoot + '/configuration.js').config ;
var config = configGen.eris;
var erisC = require('eris-contracts');
var Log = require('log'),
  log = new Log(configGen.logLevel); // debug, info, error
var request = require('request');
var timeLimit = config.timeOut;



exports.getAddress = function(address) {
  return new Promise((resolve, reject) => {
    var url = config.node0.dbUrl + '/get_account?address="' + address +
      '"';
    request(url, (error, response, body) => {
      if (!error && response.statusCode === 200) {
        var result = JSON.parse(body);
        if (result.error) {
          reject(result.error);
        } else {
          resolve(result.result[1].account);
        }
      } else {
        reject(error);
      }
    });
  });
};

exports.getContractFunction = function (abi, address, functionName, account){
  log.info('[erisdb_model] '+'getting function :');
  try {
    //Connection
    var contractManager = erisC.newContractManagerDev(config.node0.url,
      account);
    // Create factory from abi.
    var myContract = contractManager.newContractFactory(abi).at(address);/*Aquí vendría un callback para averiguar si el contrato existe o no*/
    var result = myContract[functionName];
    return result;
  } catch (e) {
    log.error('[erisdb_model] '+'error getting function ' + functionName + ': ' + e );
    return null;
  }
};

function utf8ToHex (salida, maxlength){
  var texto = salida;
  if (maxlength){
    texto = salida.substr(0, maxlength);
  }
  return new Buffer(texto, 'utf8').toString('hex');
}

function hex2Utf8 (hex){
  var cadena = hex.replace(/^(00)+/, ''); //remove leading zeros
  return new Buffer(cadena, 'hex').toString('utf8').replace(/\u0000/g, "");
}

function getTheFunctionAbi (abi, functionName){
  log.debug('[erisdb_model] '+'search funcion ABI');
  var result = abi.find ( (element) => {return element.name === functionName && element.type === 'function';});
  log.debug('[erisdb_model] '+'found function abi: ' + result);
  return result;
}

function createParamsArray( functionAbi, parameters){
  var inputs = functionAbi.inputs;
  var result = inputs.map ( input => {
    var rawValue = parameters[input.name];
    var finalValue = null;
    if (typeof(rawValue) !== 'undefined'){
      switch (input.type) {
        case 'bytes32':
          log.debug('[erisdb_model] '+'type bytes32');
          if (rawValue.length === 0 ){
            rawValue = ' ';
          }
          finalValue = utf8ToHex(rawValue,32);
          break;
        default:
          finalValue = rawValue;
      }
      log.debug('[erisdb_model] '+'att [' + input.name + '] = ' + rawValue + ' -> ' + finalValue );
    } else {
      log.debug('[erisdb_model] '+'att [' + input.name + '] not provided !!');
    }
    return finalValue;
  });
  log.debug('[erisdb_model] '+'atts '+JSON.stringify(result));
  return result;
}

function parseContractResponse (functionAbi, rawResponse){
  if (typeof(rawResponse) !== 'object' ){ //single element, wrapping it on a object to homogenize
    rawResponse = [rawResponse];
  }
  var result = {};
  functionAbi.outputs.forEach( (theOutput, theIndex) => {
    var rawResponseItem = rawResponse[theIndex];
    var procesedResponse = null ;
    var procesor = '';
    switch (theOutput.type){
      case 'bytes32':
        procesor = 'hex2utf8';
        procesedResponse = hex2Utf8(rawResponseItem);
        break;
      case 'uint':
      case 'uint256':
        procesor = 'number';
        procesedResponse = parseInt(rawResponseItem);
        break;
      default:
        procesor = 'none';
        procesedResponse = rawResponseItem;
    }
    log.debug('[erisdb_model] '+'response att '+ theOutput.name + ': ' + rawResponseItem + ' -> ' + procesedResponse + ' (' + procesor + ')');
    result[theOutput.name] = procesedResponse ;
  });
  return result;
}


exports.callContractFunction = function (abi, address, functionName, parameters, account){
  log.info('[erisdb_model] '+'calling function');
  var theFunction = exports.getContractFunction(abi, address, functionName, account);
  var theFunctionAbi = getTheFunctionAbi(abi, functionName);
  return new Promise ((resolve, reject) => {
    var timeOut = setTimeout(function(){ /*Solución temporal hasta que Eris decida implemntar comprobar que un contrato existe ya*/
      reject('Incorrect contract address');
    },timeLimit);
    if (theFunction && theFunctionAbi){
      var paramArray = createParamsArray(theFunctionAbi, parameters);
      paramArray.push(function(error, response){
        if (error){
          log.error ('[erisdb_model] '+'contract function call resulted in error: '+ error);
          reject(error);
        }
        log.info('[erisdb_model] '+'response: ' + response);
        if (typeof(response)==='undefined'){
          resolve();
        }
        else{
          var proccessedResponse = parseContractResponse(theFunctionAbi, response);
          resolve(proccessedResponse);
        }
      });
      theFunction.apply(this, paramArray);
    }
    else {
      log.error('[erisdb_model] '+'function not found in contract or contract definition');
      reject('function not found in contract or contract definition');
    }
  });
};

exports.sendTransaction = function(transaction) {
  return new Promise((resolve, reject) => {
    //package transaction for jsonrpc
    log.info('[erisdb_model] '+'broadcasting: ');
    var postParameters = {
      url: config.node0.dbUrl + '',
      form: JSON.stringify(transaction)
    };
    log.debug('[erisdb_model] '+'posting: ' + JSON.stringify(postParameters, null, 2));
    request.post(postParameters, function(error, response, body) {
      if (!error && response.statusCode === 200) {
        log.info('[erisdb_model] '+'broadccasted successfully!');
        log.debug('[erisdb_model] '+body);
        if (body.error) {
          throw new Error('error sending transaction: ' + body.error);
        }
        var addr = JSON.parse(body).result[1].receipt.contract_addr;
        log.debug('[erisdb_model] '+'addr: ' + addr);
        resolve(addr);
      } else {
        var theError = error || ('Status ' + response.statusCode +
          ' ' + response.statusMessage);
        log.error('[erisdb_model] '+theError);
        reject(theError);
      }
    });
  });
};


function retryDeployContract(myContractFactory, contract, name){
  return new Promise((resolve, reject) => {
    var intervalObject = setInterval(function(){
      log.debug('Trying deploy contract ...');
      myContractFactory.new({
        data: contract
      }, function(error, depContract) {
        log.debug('contract sent!');
        if (error) {
          log.error('error sending compiled contract: ' + error);
          reject(error);
        } else {
          log.info('contract deployed');
          log.debug(depContract.address);
          var result = {
            name: name,
            platform: 'eris',
            address: depContract.address
          }
          clearInterval(intervalObject);
          resolve(result);
        }
      });
    }, config.deployIntervalDelay);
  });
}

exports.sendCompiledContract = function(contract, abi, name) {
  console.info('sending compiled contract');
  return new Promise((resolve, reject) => {
    //Connection
    var contractManager = erisC.newContractManagerDev(config.node0.url,
      config.accountData);
    // Create factory from abi.
    var myContractFactory = contractManager.newContractFactory(abi);

    // To create a new instance and simultaneously deploy a contract use `new`
    retryDeployContract(myContractFactory, contract, name)
    .then(deployed =>{
      resolve(deployed);
    })
  });
};

function transformValue(value, type){
  var procesedResponse = '';
  switch (type){
    case 'bytes32':
      procesedResponse = hex2Utf8(value);
      break;
    case 'uint':
    case 'uint256':
      procesedResponse = parseInt(value);
      break;
    default:
      procesedResponse = value;
  }
  return procesedResponse;
}

function processEventArgs(eventArgs, eventInputs){
  var processedEventArgs = {};
  for(var i in eventArgs){
    var name = i;
    for(var j in eventInputs){
      if(name === eventInputs[j].name){
        var type = eventInputs[j].type;
        var value = eventArgs[i];
        var processedValue = transformValue(value, type);
        processedEventArgs[name] = processedValue;
      }
    }
  }
  return processedEventArgs;
}

function processEvent(abi, evento){
  var eventName = evento.event;
  var args = evento.args;
  var processedEvent = {};
  processedEvent.event = eventName;
  processedEvent.address = evento.address;
  for(var i in abi){
    if(eventName === abi[i].name){
      var processedArgs = processEventArgs(args, abi[i].inputs);
      processedEvent.args = processedArgs;
    }
  }
  return processedEvent;
}

exports.eventWatcher = function(abi, contractAddress, eventName, eventCallback){
  var subscription;
  function startCallback(error, eventSub){
       if(error){
        log.info('Retrying subscription...');
       }
       subscription = eventSub;
   }

  log.info('[erisdb_model] '+`Listening event ${eventName} .....`);
  var contractManager = erisC.newContractManagerDev(config.node0.url,
    config.accountData);
  var myContract = contractManager.newContractFactory(abi).at(contractAddress);
  var preProcess = function(err, evento){
    if(!err){
      var eventProcessed = processEvent(abi, evento);
      eventCallback(null, eventProcessed);
    }
  };
  myContract[eventName](startCallback, preProcess);
};
