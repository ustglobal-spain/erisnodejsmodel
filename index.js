'use strict';

var getImplementation = function(input){
  var imp;
  if(input === 'eth'){
    imp = require('./platforms/ethereum.js');
  }
  else{
    imp = require('./platforms/eris.js');
  }

  return imp;
}


module.exports = {
  getImplementation: getImplementation
};
